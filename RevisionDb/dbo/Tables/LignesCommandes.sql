﻿CREATE TABLE [dbo].[LignesCommandes]
(
	[IdLigneCommande] INT NOT NULL PRIMARY KEY identity ,
	IdProduit int,
	Quantite int,
	IdCommande int, 
    CONSTRAINT [FK_LignesCommandes_Produit] FOREIGN KEY (IdProduit) REFERENCES Produit(Id), 
    CONSTRAINT [FK_LignesCommandes_ToCommandes] FOREIGN KEY (IdCommande) REFERENCES Commandes(IdCommande)
)
