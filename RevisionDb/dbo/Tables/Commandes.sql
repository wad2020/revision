﻿CREATE TABLE [dbo].[Commandes]
(
	[IdCommande] INT NOT NULL PRIMARY KEY identity,
	Statut nvarchar(50) NOT NULL,
	Montant float NOT NULL,
	DateCommande datetime,
	IdClient int, 
    CONSTRAINT [FK_Commandes_ToClients] FOREIGN KEY (IdClient) REFERENCES Clients(IdClient)
)
