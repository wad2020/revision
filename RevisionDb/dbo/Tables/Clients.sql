﻿CREATE TABLE [dbo].[Clients]
(
	[IdClient] INT NOT NULL PRIMARY KEY identity,
	Adresse nvarchar(250) NOT NULL,
	Telephone nvarchar(80) NOT NULL,
	idUtilisateur int NOT NULL, 
    CONSTRAINT [FK_Clients_Utilisateurs] FOREIGN KEY (idUtilisateur) REFERENCES Utilisateur(Id)
)
