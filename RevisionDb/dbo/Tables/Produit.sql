﻿CREATE TABLE [dbo].[Produit] (
    [id]   INT           IDENTITY (1, 1) NOT NULL,
    [Nom]  NVARCHAR (50) NOT NULL,
    [Prix] FLOAT (53)    NOT NULL,
    [QteStock] INT NULL, 
    CONSTRAINT [PK_Produit] PRIMARY KEY CLUSTERED ([id] ASC)
);

