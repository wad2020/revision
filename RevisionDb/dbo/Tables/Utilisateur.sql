﻿CREATE TABLE [dbo].[Utilisateur] (
    [Id]            INT            IDENTITY (1, 1) NOT NULL,
    [Nom]           NVARCHAR (50)  NOT NULL,
    [Prenom]        NVARCHAR (50)  NOT NULL,
    [Email]         NVARCHAR (320) NOT NULL,
    [DateNaissance] DATE           NOT NULL,
    [Tel] NVARCHAR(250) NULL, 
    [Login] NVARCHAR(50) NULL, 
    [Password] NVARCHAR(250) NULL, 
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


