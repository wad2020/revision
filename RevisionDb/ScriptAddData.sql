﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

 
DELETE FROM LignesCommandes;
DELETE FROM Commandes;
DELETE FROM Clients;
DELETE FROM Utilisateur;
DELETE FROM Produit;

GO
SET IDENTITY_INSERT [dbo].[Utilisateur] ON 
GO
INSERT [dbo].[Utilisateur] ([Id], [Nom], [Prenom], [Email], [DateNaissance], [Tel], [Login], [Password]) VALUES (1, N'TEST', N'Mike', N'Person.Mike@cognitic.be', CAST(N'1980-03-17' AS Date), NULL, NULL, NULL)
GO
INSERT [dbo].[Utilisateur] ([Id], [Nom], [Prenom], [Email], [DateNaissance], [Tel], [Login], [Password]) VALUES (2, N'ly', N'Khun', N'ly.Khun@cognitic.be', CAST(N'1984-02-12' AS Date), NULL, NULL, NULL)
GO
INSERT [dbo].[Utilisateur] ([Id], [Nom], [Prenom], [Email], [DateNaissance], [Tel], [Login], [Password]) VALUES (1002, N'Mirguet', N'BErnard', N'Mirguet.BErnard@cognitic.be', CAST(N'1984-12-05' AS Date), NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Utilisateur] OFF
GO
SET IDENTITY_INSERT [dbo].[Clients] ON 
GO
INSERT [dbo].[Clients] ([IdClient], [Adresse], [Telephone], [idUtilisateur]) VALUES (1, N'Rue du colibri', N'04956356', 1)
GO
INSERT [dbo].[Clients] ([IdClient], [Adresse], [Telephone], [idUtilisateur]) VALUES (2, N'Rue du grand saint', N'48454844', 2)
GO
INSERT [dbo].[Clients] ([IdClient], [Adresse], [Telephone], [idUtilisateur]) VALUES (3, N'Allée des Geais', N'48458445', 2)
GO
INSERT [dbo].[Clients] ([IdClient], [Adresse], [Telephone], [idUtilisateur]) VALUES (4, N'Avenue du champs', N'67876876', 1002)
GO
SET IDENTITY_INSERT [dbo].[Clients] OFF
GO
SET IDENTITY_INSERT [dbo].[Commandes] ON 
GO
INSERT [dbo].[Commandes] ([IdCommande], [Statut], [Montant], [DateCommande], [IdClient]) VALUES (1, N'En cours', 120, CAST(N'2020-12-12T00:00:00.000' AS DateTime), 1)
GO
INSERT [dbo].[Commandes] ([IdCommande], [Statut], [Montant], [DateCommande], [IdClient]) VALUES (2, N'Payée', 56, CAST(N'2020-02-11T00:00:00.000' AS DateTime), 1)
GO
INSERT [dbo].[Commandes] ([IdCommande], [Statut], [Montant], [DateCommande], [IdClient]) VALUES (5, N'Annulée', 2560, CAST(N'2019-02-03T00:00:00.000' AS DateTime), 3)
GO
SET IDENTITY_INSERT [dbo].[Commandes] OFF
GO
SET IDENTITY_INSERT [dbo].[Produit] ON 
GO
INSERT [dbo].[Produit] ([id], [Nom], [Prix], [QteStock]) VALUES (1002, N'Chausettes', 4.5, 56)
GO
INSERT [dbo].[Produit] ([id], [Nom], [Prix], [QteStock]) VALUES (1003, N'Pull', 120, 10)
GO
INSERT [dbo].[Produit] ([id], [Nom], [Prix], [QteStock]) VALUES (1004, N'TV', 1280, 400)
GO
INSERT [dbo].[Produit] ([id], [Nom], [Prix], [QteStock]) VALUES (1005, N'Voiture', 8654, 2)
GO
SET IDENTITY_INSERT [dbo].[Produit] OFF
GO
SET IDENTITY_INSERT [dbo].[LignesCommandes] ON 
GO
INSERT [dbo].[LignesCommandes] ([IdLigneCommande], [IdProduit], [Quantite], [IdCommande]) VALUES (2, 1002, 26, 1)
GO
INSERT [dbo].[LignesCommandes] ([IdLigneCommande], [IdProduit], [Quantite], [IdCommande]) VALUES (3, 1002, 12, 2)
GO
INSERT [dbo].[LignesCommandes] ([IdLigneCommande], [IdProduit], [Quantite], [IdCommande]) VALUES (5, 1004, 2, 5)
GO
SET IDENTITY_INSERT [dbo].[LignesCommandes] OFF
GO

