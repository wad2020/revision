﻿using RevisionWad.Models;
using RevisionWad.Repositories;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Configuration;
namespace RevisionWad
{
    class Program
    {
        static void Main(string[] args)
        {


            Console.WriteLine("Test for Git");

            #region Avant le DataContext
            ////Utilisateur[] tabUtilisateur = new Utilisateur[10];==> Array
            ////ArrayList ==> Dynamique en taille MAIS c'est de l'objet uniquement
            //List<Utilisateur> lsUtilisateur= new List<Utilisateur>();

            //try
            //{
            //    Utilisateur monUtilisateur = new Utilisateur
            //           (
            //               "l",
            //               "Mike",
            //               "michael.person@cognitic.be",
            //               new DateTime(1982, 3, 17));


            //    lsUtilisateur.Add(monUtilisateur);


            //    Utilisateur monCollegue = new Utilisateur("Ly");
            //    monCollegue.Email = "khun.ly@cognitic.be";
            //    monCollegue.Prenom = "Khun";
            //    lsUtilisateur.Add(monCollegue);
            //}
            //catch (NullReferenceException nex)
            //{
            //    Console.WriteLine("Votre liste est nulle!!!" + nex.Message);
            //}
            //catch (ArgumentException Aex)
            //{
            //    Console.WriteLine(Aex.Message);
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex);
            //}

            //Utilisateur monCollegue2;

            //Console.WriteLine("Votre nom:");
            //string nom = Console.ReadLine();
            //Console.WriteLine("Votre prénom");
            //string prenom = Console.ReadLine();


            ////Gestion des erreurs d'encodage
            //string MessagesDerreurs = "";
            //if(nom.Length<2)
            //{
            //    MessagesDerreurs = "Veuillez encoder un nom de min 2 caractères";
            //}
            //if(prenom.Length>24)
            //{
            //    MessagesDerreurs += "\r\nVotre prénom est trop long";
            //}

            //if(MessagesDerreurs =="")
            //{
            //    try
            //    {
            //        monCollegue2 = new Utilisateur(nom);
            //        monCollegue2.Prenom = prenom;
            //    }
            //    catch (Exception ex)
            //    {
            //        Console.WriteLine(ex.Message);
            //    }
            //}
            //else
            //{
            //    Console.WriteLine(MessagesDerreurs);
            //    Console.WriteLine("Veuillez réencoder les infos");
            //} 
            #endregion

            string cnstr = ConfigurationManager.ConnectionStrings["Cnstr"].ConnectionString;

            DataContext ctx = new DataContext(cnstr);
            #region Utilisateurs
            //for (int i = 0; i < 3; i++)
            //{

            //    Console.WriteLine($"Nom de l'utilisateur n°{i}");
            //    string nom = Console.ReadLine();

            //    Console.WriteLine($"Prénom de l'utilisateur n°{i}");
            //    string prenom = Console.ReadLine();

            //    //pour des raison de facilité
            //    string email = $"{nom}.{prenom}@cognitic.be";

            //    Console.WriteLine("Votre date de naissance (dd/MM/yyyy):");
            //    string strDate = Console.ReadLine();
            //    DateTime naissance;
            //    while (!DateTime.TryParse(strDate, out naissance))
            //    {
            //        Console.WriteLine("Votre date de naissance (dd/MM/yyyy):");
            //        strDate = Console.ReadLine();
            //    }
            //    if (ctx.AddUtilisateur(nom, prenom, email, naissance))
            //    {
            //        Console.WriteLine("Ajout de l'utilisateur ok");
            //    }
            //    else
            //    {
            //        Console.WriteLine("Impossible d'ajouter l'utilisateur");
            //    }




            //    }

            #endregion

            #region Produits
            //Console.WriteLine("Nom du produit:");
            //string nomproduit = Console.ReadLine();
            //Console.WriteLine("Prix");
            //string valeurChiffre = Console.ReadLine();

            //valeurChiffre = valeurChiffre.Replace(".", ",");

            //double prix = double.Parse(valeurChiffre);

            //if (ctx.AddProduit(nomproduit, prix))
            //{
            //    Console.WriteLine("Produit ajouté");
            //}
            //else
            //{
            //    Console.WriteLine("Produit pas ajouté!");
            //}
            #endregion


            #region CLient
           // List<Client> lc = ctx.GetAllClients();

            //Utilisateur u = lc[0].Utilisateur; //uniquement ici que le chargement DB se fait! Lazy Loading 

            Client c = ctx.GetOneClient(1);





            //Utilisateur u = c.Utilisateur; //Pas d'infinity loop

            

            #endregion
            

            Console.ReadLine();


            //Utilisateur u = ctx.GetOneUtilisateur(c.IdUtilisateur); //ici non plus pas d'infinity loop

        }
         
    }
}
