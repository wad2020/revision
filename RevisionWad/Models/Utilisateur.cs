﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevisionWad.Models
{
    public class Utilisateur
    {
        //Pour créer une région :
        // 1) Sélectionner le bloc de code à mettre dans la région
        // 2) CTRL + k + s ==> Choisir #region
        // 3) Donner un nom à la région
        // Remarque : Aucun impact sur le code
        #region Fields 
        // CTRL + R + E
        private int _id;
        private string _nom, _prenom, _email, _tel, _login, _password;
        private DateTime _dateNaissance;
        #endregion
        #region Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        /// <summary>
        /// Propriété permettant d'accèder au Nom
        /// </summary>
        /// <remarks>Le nom doit contenir minimum 3 caractères</remarks>
        public string Nom
        {
            get
            {
                return _nom;
            }

            set
            {
                if (value.Length >= 2)
                {
                    _nom = value;
                }
                else
                {
                    //Je lance une exception pour prévenir de la violation
                    //de la contrainte business
                    // !!!!!ATTENTION!!!
                    // Elle doit être caturée dans le program (try-catch)
                    // sinon l'application PLANTE
                    throw new ArgumentException ("Votre nom est trop petit! Min. 2 caractères");
                }
            }
        }

        public string Prenom
        {
            get
            {
                return _prenom;
            }

            set
            {
                if (value.Length < 25)
                {
                    _prenom = value;
                }
                else
                {
                    throw new ArgumentException("Votre prénom est trop grand! (Max 25 caractères");
                }
            }
        }

        public string Email
        {
            get
            {
                return _email;
            }

            set
            {
                _email = value;
            }
        }

        public DateTime DateNaissance
        {
            get
            {
                return _dateNaissance;
            }

            set
            {
                _dateNaissance = value;
            }
        }

        public string Tel
        {
            get
            {
                return _tel;
            }

            set
            {
                _tel = value;
            }
        }

        public string Login
        {
            get
            {
                return _login;
            }

            set
            {
                _login = value;
            }
        }

        public string Password
        {
            get
            {
                return _password;
            }

            set
            {
                _password = value;
            }
        }

        public List<Client> ClientsAdresse
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

     
        #endregion

        #region Constructors

        public Utilisateur()
        {

        }

        /// <summary>
        /// Constructeur permettant de définir le nom
        /// </summary>
        /// <param name="nom">Nom de l'utilisateur</param>
        public Utilisateur(string nom)
        {
            Nom = nom;
        }
        /// <summary>
        /// Constructeur permettant de définir le nom et le prénom
        /// </summary>
        /// <param name="nom">Nom de l'utilisateur</param>
        /// <param name="prenom">Prenom de l'utilisateur</param>
        public Utilisateur(string nom, string prenom) : this(nom)
        {
            //this.Nom = nom; //Propriété = paramètre
            Prenom = prenom;
        }
        /// <summary>
        /// Constructeur permettant de définir le nom, prénom et l'email
        /// </summary>
        /// <param name="nom">Nom de l'utilisateur</param>
        /// <param name="prenom">Prénom de l'utilisateur</param>
        /// <param name="email">Email de l'utilisateur</param>
        public Utilisateur(string nom, string prenom, string email)
            : this(nom, prenom)
        {
            Email = email;
        }
        
        public Utilisateur(string nom, string prenom, string email, DateTime dateNaissance)
            : this(nom, prenom, email)
        {
            DateNaissance = dateNaissance;
        }

        #endregion


        public override string ToString()
        {
            return $"{this.Nom} {this.Prenom} - {this.DateNaissance}";
        }
    }
}
