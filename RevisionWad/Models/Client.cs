﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevisionWad.Models
{
    public class Client
    {
        
        private int _idClient, _idUtilisateur;
        private string _adresse, _telephone;

        
        public int IdClient
        {
            get
            {
                return _idClient;
            }

            set
            {
                _idClient = value;
            }
        }

        public int IdUtilisateur //FK
        {
            get
            {
                return _idUtilisateur;
            }

            set
            {
                _idUtilisateur = value;
            }
        }

        public string Adresse
        {
            get
            {
                return _adresse;
            }

            set
            {
                _adresse = value;
            }
        }

        public string Telephone
        {
            get
            {
                return _telephone;
            }

            set
            {
                _telephone = value;
            }
        }


        public List<Commandes> ClientCommandes
        {
            get {
                //Charger l'utilisateur associé au client actuel (Lazy Loading)
                //J'ai l'id de l'utilisateur associé dans la property IdUtilisateur
                //!!!!ATTENTION!!! Idéalement il faudrait éviter l'appel au datacontext ici car on a pas la connectionstring disponible
                //!!!! Sera changé plus tard 
                DataContext ctx = new DataContext(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=RevisionDb;Integrated Security=True;Connect Timeout=60;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
                return ctx.GetCommandesFromClient(this.IdClient);
            } 
        }

         

        public Utilisateur Utilisateur
        {
            get
            {
                //Charger l'utilisateur associé au client actuel (Lazy Loading)
                //J'ai l'id de l'utilisateur associé dans la property IdUtilisateur
                //!!!!ATTENTION!!! Idéalement il faudrait éviter l'appel au datacontext ici car on a pas la connectionstring disponible
                //!!!! Sera changé plus tard 
                DataContext ctx = new DataContext(ConfigurationManager.ConnectionStrings["Cnstr"].ConnectionString);
                return ctx.GetOneUtilisateur(this.IdUtilisateur);
            } 
        }
    }
}
