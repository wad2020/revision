﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevisionWad.Models
{
    public class Produit
    {
        private int _id;
        private string _nom;
        private double _prix;
        private int _qteStock;

        public string Nom
        {
            get
            {
                return _nom;
            }

            set
            {
                _nom = value;
            }
        }

        public double Prix
        {
            get
            {
                return _prix;
            }

            set
            {
                if (value > 0)
                {
                    _prix = value;
                }
                else
                {
                    throw new InvalidOperationException("Le prix doit être positif");
                }
            }
        }

        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }

        public int QteStock
        {
            get
            {
                return _qteStock;
            }

            set
            {
                _qteStock = value;
            }
        }

        public Produit()
        {

        }

        public Produit(string nom, double prix)
        {
            Nom = nom;
            Prix = prix;
        }


        public List<LignesCommandes> LignesCommandes
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }
    }
}
