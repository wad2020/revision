﻿using RevisionWad.Repositories;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevisionWad.Models
{
    public class DataContext
    {
        #region Fields
        string _connectionString; 
        IConcreteRepository<Utilisateur> _userRepo;
        IConcreteRepository<Produit> _produitRepo;
        IConcreteRepository<Client> _clientsRepo;
        IConcreteRepository<LignesCommandes> _ligneCmdRepo;
        IConcreteRepository<Commandes> _commandRepo;
        #endregion

        #region Properties


        #endregion
        /// <summary>
        /// Constructeur vide qui initialise la liste
        /// </summary>
        public DataContext(string connectionstring)
        {
            
            _userRepo = new UtilisateurRepository(connectionstring);
            _produitRepo = new ProduitRepository(connectionstring);
            _clientsRepo = new ClientRepository(connectionstring);
            _commandRepo = new CommandesRepository(connectionstring);
            _ligneCmdRepo = new LignesCommandesRepository(connectionstring);
            this._connectionString = connectionstring;
        }

        #region Method

        #region Utilisateur
        /// <summary>
        /// Ajoute un utilisateur en vérifiant que l'on respecte les
        /// contraintes business
        /// </summary>
        /// <param name="nom">Nom de l'utilisateur (min 2 caract.)</param>
        /// <param name="prenom">Prenom de l'utilisateur (max 25 caract.)</param>
        /// <param name="email">L'email de l'utilisateur</param>
        /// <param name="dateNaissance">La date de naissance de l'utilisateur</param>
        /// <returns>False si l'ajout n'a pas fonctionné</returns>
        public bool AddUtilisateur(string nom, string prenom, string email, DateTime dateNaissance, string login,string password, string tel)
        {
            if (nom.Length < 2 || prenom.Length > 24)
            {
                Debug.WriteLine("Non respect des tailles");
                return false;
            }

            try
            {
                Utilisateur user = new Utilisateur()
                {
                    Email = email,
                    Nom = nom,
                    Prenom = prenom,
                    DateNaissance = dateNaissance,
                    Login =login,
                    Password =password,
                    Tel =tel
                };

                return _userRepo.Insert(user);
            }
            catch (NullReferenceException nex)
            {
                Debug.WriteLine("Votre liste est nulle!!!" + nex.Message);
                return false;
            }
            catch (ArgumentException Aex)
            {
                Debug.WriteLine(Aex.Message);
                return false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                return false;
            }
        }

      

        public bool UpdateUtilisateur(Utilisateur u)
        {
            return _userRepo.Update(u);
        }

        public bool DeleteUser(Utilisateur u)
        {
            return _userRepo.Delete(u);
        }
        public Utilisateur GetOneUtilisateur(int id)
        { 
            return _userRepo.GetOne(id); //id ==> PK
        }
        public List<Utilisateur> GetAllUsers()
        {
            return _userRepo.Get();
        }
        
        #endregion
        #region Produit

        /// <summary>
        /// Permet d'ajouter un produit
        /// </summary>
        /// <param name="nom"></param>
        /// <param name="prix"></param>
        /// <returns></returns>
        public bool AddProduit(string nom, double prix, int qteStock)
        {
            if (prix < 0)
            {
                Debug.WriteLine("Prix négatif");
                return false;
            }

            try
            {
                Produit p = new Produit(nom, prix);
                p.QteStock = qteStock;
                return _produitRepo.Insert(p); 
            }
            catch (Exception)
            {
                return false;
            }
        }
       
        public bool UpdateProduit(Produit toUpdate)
        {
            return _produitRepo.Update(toUpdate);
        }

        public bool DeleteProduit(Produit toDelete)
        {
            return _produitRepo.Delete(toDelete);
        }
        
        public Produit GetOneProduit(int id)
        { 
            return _produitRepo.GetOne(id);
        }
        public List<Produit> GetAllProduit()
        {
            return _produitRepo.Get();
        }

        #endregion
        #region Client
        public List<Client> GetAllClients()
        {
            return _clientsRepo.Get();
        }

        public Client GetOneClient(int pk)
        {  
            return _clientsRepo.GetOne(pk);
        }

       
        #endregion
        #region Ligne de commandes

        public LignesCommandes GetOneLigneCommandes(int PK)
        {
            return _ligneCmdRepo.GetOne(PK);
        }
        public List<LignesCommandes> GetCurrentLignesDeCommande(int idCommande)
        {
            return ((LignesCommandesRepository)_ligneCmdRepo).GetFromCommande(idCommande);
        }
        #endregion

        #region Commande

        public Commandes GetOneCommande(int PK)
        {
            return _commandRepo.GetOne(PK);
        }
        public List<Commandes> GetCommandesFromClient(int idClient)
        {
            return ((CommandesRepository)_commandRepo).GetCommandesFromClient(idClient);
        }

        #endregion
        #endregion
    }
}
