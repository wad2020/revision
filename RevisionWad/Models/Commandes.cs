﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevisionWad.Models
{
    public class Commandes
    {
        #region Fields ~=> Collumn
        private int _idCommande;
        private string _statut;
        private double _montant;
        private DateTime _dateCommande;
        private int _idClient;
        #endregion

        public int IdCommande
        {
            get
            {
                return _idCommande;
            }

            set
            {
                _idCommande = value;
            }
        }

        public string Statut
        {
            get
            {
                return _statut;
            }

            set
            {
                _statut = value;
            }
        }

        public double Montant
        {
            get
            {
                return _montant;
            }

            set
            {
                _montant = value;
            }
        }

        public DateTime DateCommande
        {
            get
            {
                return _dateCommande;
            }

            set
            {
                _dateCommande = value;
            }
        }

        public int IdClient
        {
            get
            {
                return _idClient;
            }

            set
            {
                _idClient = value;
            }
        }

        #region Navigation Property
        public List<LignesCommandes> LignesCommande
        {
            get
            {   //!!!!ATTENTION!!! Idéalement il faudrait éviter l'appel au datacontext ici car on a pas la connectionstring disponible
                //!!!! Sera changé plus tard 
                DataContext ctx = new DataContext(ConfigurationManager.ConnectionStrings["Cnstr"].ConnectionString);
                return ctx.GetCurrentLignesDeCommande(IdCommande);
            }
             
        }

        #endregion

        public Client Client
        {
            get
            {   //!!!!ATTENTION!!! Idéalement il faudrait éviter l'appel au datacontext ici car on a pas la connectionstring disponible
                //!!!! Sera changé plus tard 
                DataContext ctx = new DataContext(ConfigurationManager.ConnectionStrings["Cnstr"].ConnectionString);
                return ctx.GetOneClient(IdClient);
            }
        }

       
    }
}
