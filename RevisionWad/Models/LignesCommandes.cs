﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevisionWad.Models
{
    public class LignesCommandes
    {

        // select *
        //FROM LignesCommandes
        private int _idLigneCommande, _idProduit, _idCommande;
        private int _qte;

        public int IdProduit
        {
            get
            {
                return _idProduit;
            }

            set
            {
                _idProduit = value;
            }
        }

        public int IdCommande
        {
            get
            {
                return _idCommande;
            }

            set
            {
                _idCommande = value;
            }
        }

        public int Qte
        {
            get
            {
                return _qte;
            }

            set
            {
                _qte = value;
            }
        }


        public int IdLigneCommande
        {
            get
            {
                return _idLigneCommande;
            }

            set
            {
                _idLigneCommande = value;
            }
        }



        #region Navigation
        public Commandes Commandes //Inner join Commandes on Commandes.idCommandes = LignesCommandes.idCommandes
        {
            get
            {
                //!!!!ATTENTION!!! Idéalement il faudrait éviter l'appel au datacontext ici car on a pas la connectionstring disponible
                //!!!! Sera changé plus tard 
                DataContext ctx = new DataContext(ConfigurationManager.ConnectionStrings["Cnstr"].ConnectionString);
                return ctx.GetOneCommande(IdCommande);
            }

            
        }

        public Produit Produit //navigation Property ==>  inner join Produit on Produit.idProduit = LignesCommandes.idProduit
        {
            get
            {
                DataContext ctx = new DataContext(ConfigurationManager.ConnectionStrings["Cnstr"].ConnectionString);
                return ctx.GetOneProduit(IdProduit);
            } 
        }

      
        #endregion
    }
}
