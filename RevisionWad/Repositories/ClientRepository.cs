﻿using RevisionWad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevisionWad.Repositories
{
    public class ClientRepository : BaseRepository<Client>, IConcreteRepository<Client>
    {
        public ClientRepository(string connectionString) : base(connectionString)
        {

        }


        public List<Client> Get()
        {
            string requete = "Select * from Clients";

            return base.Get(requete);
        }

        public Client GetOne(int PK)
        {
            string requete = "Select * from Clients where IdClient = @id";

            return base.GetOne(PK, requete);
        }

     
        
             
        public bool Insert(Client toInsert)
        {
            string requete = @"INSERT INTO [dbo].[Clients]
                               ([IdClient]
                               ,[Adresse]
                               ,[Telephone]
                               ,[idUtilisateur])
                         VALUES
                               ( 
                               @Adresse 
                               ,@Telephone 
                               ,@idUtilisateur)";
            return base.Insert(toInsert, requete);
        }

        public bool Update(Client toUpdate)
        {
            string requete = @"UPDATE [dbo].[Clients]
                               SET [Adresse] = @Adresse 
                                  ,[Telephone] = @Telephone 
                                  ,[idUtilisateur] = @idUtilisateur 
                             WHERE IdClient = @IdClient";
            return base.Update(toUpdate, requete);
        }

        public bool Delete(Client toDelete)
        {
            string requete = @"delete from Clients WHERE IdClient =@IdClient)";
            return base.Delete(toDelete, requete);
        }
 
    }
}
