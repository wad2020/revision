﻿using RevisionWad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevisionWad.Repositories
{
    public class LignesCommandesRepository : BaseRepository<LignesCommandes>, IConcreteRepository<LignesCommandes>
    {
        public LignesCommandesRepository(string connectionString) : base(connectionString)
        {
        }

     

        public List<LignesCommandes> Get()
        {
            throw new NotImplementedException();
        }

        public LignesCommandes GetOne(int PK)
        {
            string requete = "Select * from LignesCommandes WHERE [IdLigneCommande] = @id";
            return base.GetOne(PK, requete);
        }

        public List<LignesCommandes> GetFromCommande(int IdCommande)
        {
            string requete = "select * from LignesCommandes Where IdCommande = " + IdCommande;
            return Get(requete);
        }

        public bool Insert(LignesCommandes toInsert)
        {
            //Faire mon insertion
            //La requête parametrée
            string requete = @"INSERT INTO [dbo].[LignesCommandes]
                                           ([IdProduit]
                                           ,[Quantite]
                                           ,[IdCommande])
                                     VALUES
                                           (@IdProduit 
                                           ,@Quantite 
                                           ,@IdCommande )";
            return base.Insert(toInsert, requete);
        }

        public bool Update(LignesCommandes toUpdate)
        {
            string requete = @"UPDATE [dbo].[LignesCommandes]
                               SET [IdProduit] = @IdProduit
                                  ,[Quantite] = @Quantite
                                  ,[IdCommande] = @IdCommande
                             WHERE IdLigneCommande=@IdLigneCommande";
            return base.Update(toUpdate, requete);
        }

        public bool Delete(LignesCommandes toDelete)
        {
            string requete = @"DELETE FROM [dbo].[LignesCommandes]                                
                             WHERE IdLigneCommande=@IdLigneCommande";
            return base.Update(toDelete, requete);
        }
    }
}
