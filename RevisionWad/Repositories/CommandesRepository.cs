﻿using RevisionWad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevisionWad.Repositories
{
    public class CommandesRepository : BaseRepository<Commandes>, IConcreteRepository<Commandes>
    {
        public CommandesRepository(string connectionString) : base(connectionString)
        {

        }

        

        public List<Commandes> Get()
        {
            string requete = "Select * from Commandes";
            return base.Get( requete);
        }

        public Commandes GetOne(int PK)
        {
            string requete = "Select * from Commandes where [IdCommande]=@id";
            return base.GetOne(PK, requete);
        }

        public bool Insert(Commandes toInsert)
        {
            string requete = @"INSERT INTO [dbo].[Commandes]
                               ([Statut]
                               ,[Montant]
                               ,[DateCommande]
                               ,[IdClient])
                         VALUES
                               (@Statut
                               , @Montant
                               , @DateCommande
                               , @IdClient)";
            return base.Insert(toInsert, requete);
        }

        public bool Update(Commandes toUpdate)
        {
            string requete = @"UPDATE [dbo].[Commandes]
                               SET [Statut] = @Statut 
                                  ,[Montant] = @Montant 
                                  ,[DateCommande] = @DateCommande 
                                  ,[IdClient] = @IdClient
                             WHERE IdCommande = @IdCommande";
            return base.Update(toUpdate, requete);
        }
        public bool Delete(Commandes toDelete)
        {
            string requete = "Delete  from Commandes where [IdCommande]=@IdCommande";
            return base.Delete(toDelete, requete);
        }

        public List<Commandes> GetCommandesFromClient(int IdClient)
        {
            string requete = "Select * from Commandes where [IdClient]="+ IdClient;
            return base.Get( requete);
        }
    }
}
