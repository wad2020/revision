﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RevisionWad.Models;
namespace RevisionWad.Repositories
{
    /// <summary>
    /// Une classe permettant d'effectuer le CRUD
    /// --> Insert : insertion d'un utilisateur dans la db
    /// --> Update : mise à jour d'un utilisateur dans la db
    /// --> Delete : suppression d'un utilisateur dans le db
    /// --> *select* : 
    ///         --> get : permet de récupérer tout les utilisateurs de la db
    ///         --> getOne: permet de récupérer 1! utilisateur basé sur sa PK
    /// </summary>
    public class UtilisateurRepository : BaseRepository<Utilisateur>, IConcreteRepository<Utilisateur>
    {
         
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionString"></param>
        public UtilisateurRepository(string connectionString) : base(connectionString)
        {             
             
        }
        public string InsulteLeclient()
        {
            return "*****";
        }

        #region Public

        #region Modifications
        public bool Insert(Utilisateur toInsert)
        {
            //Faire mon insertion
            //La requête parametrée
            string requete = @"INSERT INTO [dbo].[Utilisateur]
                                                       ([Nom]
                                                       ,[Prenom]
                                                       ,[Email]
                                                       ,[DateNaissance]
                                                       , Tel
                                                       , Login
                                                       , Password)
                                                 VALUES
                                                       (@Nom 
                                                       ,@Prenom
                                                       ,@Email
                                                       ,@DateNaissance
                                                       ,@Tel
                                                       ,@Login
                                                       ,@Password)";
            return base.Insert(toInsert, requete);            

          
        }
        public bool Update(Utilisateur toUpdate)
        {
            string requete = @"UPDATE [dbo].[Utilisateur]
                                   SET [Nom] = @Nom
                                      ,[Prenom] = @Prenom
                                      ,[Email] = @Email
                                      ,[DateNaissance] = @DateNaissance
                                      ,[Tel] = @Tel
                                      ,[Login] = @Login
                                      ,[Password] = @Password
                                 WHERE Id=@id";
            return base.Update(toUpdate, requete);


            #region Obsolete/Deprecated
            //if (Connect())
            //{
            //    Faire la mise à jour
            //    La requête parametrée

            //    Création de la commande
            //    SqlCommand oCmd = new SqlCommand(requete, connection);

            //    Créer les paramètres à la commande
            //    Créer les paramètres à la commande
            //    SqlParameter paramNom = new SqlParameter("Nom", toUpdate.Nom);
            //    SqlParameter paramPrenom = new SqlParameter("Prenom", toUpdate.Prenom);
            //    SqlParameter paramEmail = new SqlParameter("Email", toUpdate.Email);
            //    SqlParameter paramDateNaissance = new SqlParameter("DateNaissance", toUpdate.DateNaissance);
            //    SqlParameter paramTel = new SqlParameter("Tel", toUpdate.Tel);
            //    SqlParameter paramId = new SqlParameter("id", toUpdate.Id);

            //    Ajout à la commande
            //    oCmd.Parameters.Add(paramNom);
            //    oCmd.Parameters.Add(paramPrenom);
            //    oCmd.Parameters.Add(paramEmail);
            //    oCmd.Parameters.Add(paramDateNaissance);
            //    oCmd.Parameters.Add(paramTel);
            //    oCmd.Parameters.Add(paramId);

            //    bool isUpdated;
            //    Exécution de la commande d'insertion
            //    try
            //    {
            //        oCmd.ExecuteNonQuery();
            //        isUpdated = true;

            //    }
            //    catch (Exception)
            //    {
            //        logging d'erreur et d'exception(Voir par la suite ensemble) ==> Exemple EventViewer, Nlog, Log4net
            //       isUpdated = false;
            //    }

            //    fermer ma connexion vers la DB
            //    Disconnect();
            //    renvoyer le résultat de l'insertion
            //    return isUpdated;

            //}
            //else
            //{
            //    return false;
            //}
            #endregion
        }
        public bool Delete(Utilisateur toDelete)
        {
            string requete = @"DELETE FROM  [dbo].[Utilisateur]
                                 WHERE Id=@id";

            return base.Delete(toDelete, requete);

            #region Obsolete/Deprecated
            //if (Connect())
            //{
            //    //Faire la suppression
            //    //La requête parametrée
            //    string requete = @"DELETE FROM  [dbo].[Utilisateur]
            //                     WHERE Id=@id";
            //    //Création de la commande
            //    SqlCommand oCmd = new SqlCommand(requete, connection);

            //    //Créer les paramètres à la commande 
            //    SqlParameter paramId = new SqlParameter("id", toDelete.Id);

            //    //Ajout à la commande 
            //    oCmd.Parameters.Add(paramId);

            //    bool isDeleted;
            //    //Exécution de la commande de suppression
            //    try
            //    {
            //        oCmd.ExecuteNonQuery();
            //        isDeleted = true;

            //    }
            //    catch (Exception)
            //    {
            //        //logging d'erreur et d'exception (Voir par la suite ensemble) ==> Exemple EventViewer, Nlog, Log4net
            //        isDeleted = false;
            //    }

            //    //fermer ma connexion vers la DB
            //    Disconnect();
            //    //renvoyer le résultat de la suppression
            //    return isDeleted;

            //}
            //else
            //{
            //    return false;
            //} 
            #endregion
        }
        #endregion

        #region Recupérations
        public List<Utilisateur> Get()
        { 
                string requete = "Select * FROM Utilisateur";                 
                return base.Get(requete);
             
        }
        public Utilisateur GetOne(int PK)
        {
            //Faire ma requête select  
            string requete = "Select * FROM Utilisateur where Id=@id";
            return base.GetOne(PK, requete);
        }
        #endregion
        #endregion
      


    }
}
