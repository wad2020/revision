﻿using RevisionWad.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevisionWad.Repositories
{
    public class ProduitRepository : BaseRepository<Produit>, IConcreteRepository<Produit>
    {
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionString"></param>
        public ProduitRepository(string connectionString):base(connectionString)
        { 
        }


        #region Public

        #region Modifications
        public bool Insert(Produit toInsert)
        {
            string insertRequete = @"INSERT INTO [dbo].[Produit]
                                       ([Nom]
                                       ,[Prix], [QteStock])
                                            VALUES
                                       (@Nom, @Prix @QteStock)";
            return base.Insert(toInsert, insertRequete);


            #region Obsolete/deprecated
            //if (Connect())
            //{
            //    //!!!! les nombres décimaux s'enregistrent avec un . et non pas avec une ,
            //    // ça dépend de notre "culture" et de notre sql serveur

            //    string insertRequete = @"INSERT INTO [dbo].[Produit]
            //                           ([Nom]
            //                           ,[Prix])
            //                                VALUES
            //                           (@Nom, @Prix)";

            //    SqlParameter paramNom = new SqlParameter("Nom", toInsert.Nom);
            //    SqlParameter paramPrix = new SqlParameter("Prix", toInsert.Prix);

            //    SqlCommand oCmd = new SqlCommand(insertRequete, connection);

            //    oCmd.Parameters.Add(paramNom);
            //    oCmd.Parameters.Add(paramPrix);
            //    bool isInserted = false;
            //    try
            //    {
            //        int info = oCmd.ExecuteNonQuery();
            //        isInserted = true;
            //    }
            //    catch (Exception ex)
            //    {

            //        isInserted = false;
            //    }

            //    //fermer ma connexion vers la DB
            //    Disconnect();
            //    //renvoyer le résultat de l'insertion
            //    return isInserted;

            //}
            //else
            //{
            //    return false;
            //} 
            #endregion


        }
        public bool Update(Produit toUpdate)
        {
            string UpdateRequete = @"UPDATE [dbo].[Produit]
                                         SET [Nom] = @Nom 
                                            ,[Prix] = @Prix ,
                                              [QteStock]=@QteStock
                                         WHERE Id=@id";
            return base.Update(toUpdate, UpdateRequete);
            #region Obsolete/Deprecated
            //if (Connect())
            //{
            //    //!!!! les nombres décimaux s'enregistrent avec un . et non pas avec une ,
            //    // ça dépend de notre "culture" et de notre sql serveur

            //    string insertRequete = @"UPDATE [dbo].[Produit]
            //                             SET [Nom] = @Nom 
            //                                ,[Prix] = @Prix 
            //                             WHERE Id=@id";

            //    SqlParameter paramNom = new SqlParameter("Nom", toUpdate.Nom);
            //    SqlParameter paramPrix = new SqlParameter("Prix", toUpdate.Prix);
            //    SqlParameter paramId = new SqlParameter("id", toUpdate.Id);

            //    SqlCommand oCmd = new SqlCommand(insertRequete, connection);

            //    oCmd.Parameters.Add(paramNom);
            //    oCmd.Parameters.Add(paramPrix);
            //    oCmd.Parameters.Add(paramId);
            //    bool IsUpdated = false;
            //    try
            //    {
            //        int info = oCmd.ExecuteNonQuery();
            //        IsUpdated = true;
            //    }
            //    catch (Exception ex)
            //    {

            //        IsUpdated = false;
            //    }

            //    //fermer ma connexion vers la DB
            //    Disconnect();
            //    //renvoyer le résultat de la mise à jour
            //    return IsUpdated;

            //}
            //else
            //{
            //    return false;
            //} 
            #endregion

        }
        public bool Delete(Produit toDelete)
        {

            string DeleteRequete = @"DELETE FROM [dbo].[Produit] 
                                     WHERE Id=@id";

            return base.Delete(toDelete, DeleteRequete);


            #region Obsolete/Deprecated
            //if (Connect())
            //{
            //    //!!!! les nombres décimaux s'enregistrent avec un . et non pas avec une ,
            //    // ça dépend de notre "culture" et de notre sql serveur

            //    string insertRequete = @"DELETE FROM [dbo].[Produit] 
            //                         WHERE Id=@id";

            //    SqlParameter paramId = new SqlParameter("id", toDelete.Id);

            //    SqlCommand oCmd = new SqlCommand(insertRequete, connection);

            //    oCmd.Parameters.Add(paramId);
            //    bool IsDeleted = false;
            //    try
            //    {
            //        int info = oCmd.ExecuteNonQuery();
            //        IsDeleted = true;
            //    }
            //    catch (Exception ex)
            //    {

            //        IsDeleted = false;
            //    }

            //    //fermer ma connexion vers la DB
            //    Disconnect();
            //    //renvoyer le résultat de la suppression
            //    return IsDeleted;

            //}
            //else
            //{
            //    return false;
            //} 
            #endregion

        }
        #endregion

        #region Recupérations
        public List<Produit> Get()
        { 
            string requete = "Select * from produit";
            return base.Get(requete);
        }
        public Produit GetOne(int PK)
        {
            string requete = "Select * from produit where Id=@id";
            return base.GetOne(PK, requete);
             
        }
        #endregion
        #endregion
 


    }
}